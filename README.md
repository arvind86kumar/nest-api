
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev


## I have used sequelize for Mysql connection,
## Create connection in app.module.ts
## Create Create User form Dto for User Model for data base table design and interration.

## Create User Service and inject user model in service and use query for inserting 
http://localhost:3000/form
[
  {
    "msg": "New User form  Created Successfully",
    "statusCode": 201
  }
]

##fetching data
http://localhost:3000/fill_data?form_title=event1

[
  {
    "res": [
      {
        "id": 12,
        "uniqueId": "0a293fe1-a75e-4d6b-9ae5-49b5e0eb451e",
        "title": "Event1",
        "name": "Rohit",
        "email": "test@example.com",
        "phonenumber": 123456,
        "isGraduate": true,
        "createdAt": "2024-04-10T03:26:44.000Z",
        "updatedAt": "2024-04-10T03:26:44.000Z"
      },
      {
        "id": 13,
        "uniqueId": "9d76fea0-ebbc-4659-8de2-007d0a8ecc37",
        "title": "Event1",
        "name": "Sumit",
        "email": "test2@example.com",
        "phonenumber": 2147483647,
        "isGraduate": true,
        "createdAt": "2024-04-10T03:27:12.000Z",
        "updatedAt": "2024-04-10T03:27:12.000Z"
      }
    ],
    "statusCode": 200
  }
]
## Create Route in controller Post route(form) for form data insert , I have added validation for that each field
## Get route fill_data will fetch data from db and display in response. We have to pass form_title in query string.

## Post (fill_data) for fetching form record from user table and put it to another table (formdata).

http://localhost:3000/fill_data?form_title=event1

[
  {
    "msg": "Data inserted successfully",
    "statusCode": 201
  }
]