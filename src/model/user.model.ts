import { UUID } from 'crypto';
import { Column, Model, Table } from 'sequelize-typescript';
import {v4 as uuidv4} from 'uuid';
@Table
export class User extends Model {
  @Column({ defaultValue:uuidv4 })
  uniqueId: UUID;
  @Column
  title: string;

  @Column
  name: string;
  
  @Column
  email: string;

  @Column
  phonenumber:string;

  @Column({ defaultValue: true })
  isGraduate: boolean;
}