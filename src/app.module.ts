import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './model/user.model';
import { UsersController } from './users/users.controller';
import { UsersModule } from './users/users.module';

@Module({
  imports: [SequelizeModule.forRoot({
    dialect: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'test',
    models: [User],
    autoLoadModels: true,
    synchronize: true,
  }),  UsersModule,], 
  controllers: [AppController, UsersController],
  providers: [AppService],
})
export class AppModule {}
