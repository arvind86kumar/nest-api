import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from 'src/model/user.model';
import { FormData } from 'src/model/formdata.model';
import { UsersController } from './users.controller';

@Module({
  imports: [SequelizeModule.forFeature([User,FormData])],
  providers: [UsersService],
  controllers: [UsersController],
  exports:[UsersService]
})
export class UsersModule {
  constructor(private userService: UsersService) {}
}
