import { IsEmail, IsNotEmpty, IsNumber, IsNumberString } from 'class-validator';
export class CreateUserFormDto{   
  
    @IsNotEmpty()
    title:string;
    @IsNotEmpty()
    name:string;
   @IsNumberString()
   phonenumber
    @IsEmail()
    email:string;
    @IsNotEmpty()
    isGraduate:boolean

}