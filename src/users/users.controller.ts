import { Controller, Get,Param,Delete,Post,Body, HttpCode, Req } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserFormDto } from './create-user-form.dto';
import { Request } from 'express';
@Controller('')
export class UsersController {
  constructor(private readonly userService:UsersService){}
    
    @Get('fill_data')
    async findformData(@Req() request: Request) : Promise<Object> {
      let param = request.query.form_title;
      var data =await this.userService.getFormData(`${param}`)
      try{
        return [{
          res : data,
          statusCode:200
        }];

      }catch(err){
        return [{
          msg:'Oops some thing went wrong',
          statusCode:404
        }]
      }
    }

    @Post('fill_data')
    async fillForm(@Req() request: Request):Promise<Object> {
      let param = request.query.form_title;
      var data =await this.userService.getFormEntires(`${param}`)
      let recordCount = Object.entries(data).length;
        if(recordCount > 0)
          {
            this.userService.create(data)
            return [{
              msg:'Data inserted successfully',
              statusCode:201
            }];
          }else{
            return [{
              msg:'Please check data insert issues.',
              statusCode:204
            }];
          }
   }

  
  @Post('form')
   newForm(@Body() createNewUserForm:CreateUserFormDto):any {
     if(this.userService.newForm(createNewUserForm))
      {        
        return [{
          msg:`New User form  Created Successfully`,
          statusCode:201
        }]
      }else{
        return [{
          msg:'Oops! Something went wrong, Pls try again',
          statusCode:402
        }]
      }
  }

}
