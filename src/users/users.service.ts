import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from 'src/model/user.model';
import { FormData } from 'src/model/formdata.model';
import { error } from 'console';
@Injectable()
export class UsersService {
    constructor(
        @InjectModel(User)
      
        private userModel: typeof User,
        
      
      ) {}
      async findAll(): Promise<User[]> {
        return this.userModel.findAll();
      }
    
      async getFormData(title: string): Promise<Object> {       
        return await this.userModel.findAll({where: {
            title:title
          },
        });
      }

      async getFormEntires(title: string): Promise<Object> {       
        return await this.userModel.findAll({ attributes:['name','email','uniqueId','phonenumber','isGraduate'],raw:true,where:{
          title:title
        }});
      }

    

      async newForm(user):Promise<Object>{
        const jane = await User.create(user);
        return 1;
      }

      async create(data):Promise<Object>{
       try{
        await FormData.bulkCreate(data).then(data=>{
          return 1;
        });
       }catch(error){
        return 0
       }
       
      }
    
     
}
